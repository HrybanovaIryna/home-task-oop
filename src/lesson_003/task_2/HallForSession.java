package lesson_003.task_2;

import java.util.Arrays;

public class HallForSession {

    private final Seat[][] seats;

    public HallForSession(int row, int places) {
        seats = new Seat[row][places];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < places; j++) {
                seats[i][j] = new Seat();
            }
        }
    }

    public int getFreePlacesCount() {
        int count = 0;
        for (Seat[] seatColumn : seats) {
            for (Seat seat : seatColumn)
                if (!seat.isBooked()) {
                    count++;
                }
        }
        return count;
    }

    public int getBookedCount() {
        return seats.length * seats[0].length - getFreePlacesCount();
    }

    public boolean book(int row, int place) {
        row--;
        place--;
        if (row >= seats.length || place >= seats[0].length || row < 0 || place < 0) {
            return false;
        }

        Seat seat = seats[row][place];
        if (seat.isBooked()) {
            return false;
        }

        seat.setBooked(true);
        return true;
    }

    @Override
    public String toString() {
        return "HallForSession{" +
                "seats=" + Arrays.deepToString(seats) +
                '}';
    }
}
