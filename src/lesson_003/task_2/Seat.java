package lesson_003.task_2;

public class Seat {

    private boolean booked;

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    @Override
    public String toString() {
        return "Seat{" +
                "booked=" + booked +
                '}';
    }
}
