package lesson_003.task_2;

import java.time.LocalTime;

public class CinemaRunner {
    public static void main(String[] args) {

        Movie movie = new Movie("Bohemian rhapsody", 2018, 7980,
                "is a movie based on the true story of Queen's journey from the start of the rock band to " +
                        "their now-legendary 1985 performance at the Live Aid concert");
        HallForSession hall = new HallForSession(10, 10);
        MovieSession session = new MovieSession(movie, hall, 75, LocalTime.parse("20:30"));

        System.out.println(session);
        System.out.println("Free places count: " + hall.getFreePlacesCount());
        System.out.println("Booked places count: " + hall.getBookedCount());

        System.out.println("Booked place: " + hall.book(2, 5));
        System.out.println("Booked place: " + hall.book(2, 5));
        System.out.println("Booked places count: " + hall.getBookedCount());

        System.out.println("Session fees: " + session.sessionFees());
    }
}
