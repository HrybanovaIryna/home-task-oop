package lesson_003.task_2;

import java.time.LocalTime;

public class MovieSession {

    private final Movie movie;
    private final HallForSession hall;
    private final int price;
    private final LocalTime startTime;

    public MovieSession(Movie movie, HallForSession hall, int price, LocalTime startTime) {
        this.movie = movie;
        this.hall = hall;
        this.price = price;
        this.startTime = startTime;
    }

    public long sessionFees() {
        return price * hall.getBookedCount();
    }

    @Override
    public String toString() {
        return "MovieSession{" +
                "movie=" + movie +
                ", hall=" + hall +
                ", price=" + price +
                ", startTime=" + startTime +
                '}';
    }
}
