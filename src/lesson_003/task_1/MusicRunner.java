package lesson_003.task_1;

public class MusicRunner {
    public static void main(String[] args) {
        Song song1 = new Song("Born Again", "Beast in black", "power metal", 3, new AudioContent(new byte[2]));
        Song song2 = new Song("Magic Forest", "Amberian dawn", "symphonic metal", 5, new AudioContent(new byte[3]));
        Song song3 = new Song("Living on the run", "H.e.a.t", "hard rock", 5, new AudioContent(new byte[4]));
        Song song4 = new Song("Never give up", "Sunstorm", "hard rock", 3, new AudioContent(new byte[5]));
        Song song5 = new Song("Born this way", "Lady Gaga", "pop", 3, new AudioContent(new byte[6]));

        User user = new User("John");
        System.out.println(user.getName());

        String playlistTitle = "John`s new playlist";
        user.createPlaylist(playlistTitle);

        user.addSongToPlaylist(playlistTitle, song1);
        user.addSongToPlaylist(playlistTitle, song2);
        user.addSongToPlaylist(playlistTitle, song3);
        user.addSongToPlaylist(playlistTitle, song4);
        user.addSongToPlaylist(playlistTitle, song5);

        Playlist playlist = user.findByTitle(playlistTitle);
        System.out.println(playlist.getTitle());
        System.out.println(playlist.getTotalLength());
        System.out.println(playlist.findByTitlePart("Born"));
        System.out.println(playlist.findByAuthor("H.e.a.t"));

        System.out.println(song1.getContent());

        System.out.println(song2.getTitle());
        System.out.println(song2.getAuthor());
        System.out.println(song2.getGenre());
        System.out.println(song2.getLength());
        System.out.println(song2.getContent());
    }
}
