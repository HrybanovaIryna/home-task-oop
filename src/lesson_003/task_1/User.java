package lesson_003.task_1;

import java.util.ArrayList;

public class User {
    private final String name;
    private final ArrayList<Playlist> playlists = new ArrayList<>();

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Playlist createPlaylist(String title) {
        try {
            findByTitle(title);
            throw new IllegalArgumentException("Playlist with title " + title + " already in users list");
        } catch (IllegalArgumentException e) {
            Playlist playlist = new Playlist(title);
            playlists.add(playlist);
            return playlist;
        }
    }

    public Playlist findByTitle(String title) {

        for (Playlist playlist : playlists) {
            if (playlist.getTitle().equals(title)) {
                return playlist;
            }
        }
        throw new IllegalArgumentException("no playlist with title " + title);
    }

    public void addSongToPlaylist(String title, Song song) {
        Playlist playlist = findByTitle(title);
        playlist.addSong(song);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", playlists=" + playlists +
                '}';
    }
}
