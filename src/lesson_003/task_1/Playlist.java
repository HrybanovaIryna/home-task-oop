package lesson_003.task_1;

import java.util.ArrayList;

public class Playlist {
    private final String title;
    private final ArrayList<Song> songs = new ArrayList<>();

    public Playlist(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void addSong(Song song) {
        songs.add(song);
    }

    public long getTotalLength() {
        long length = 0;
        for (Song song : songs) {
            length += song.getLength();
        }
        return length;
    }

    public ArrayList<Song> findByTitlePart(String title) {
        ArrayList<Song> result = new ArrayList<>();

        for (Song song : songs) {
            if (song.getTitle().contains(title)) {
                result.add(song);
            }
        }
        return result;
    }

    public ArrayList<Song> findByAuthor(String author) {
        ArrayList<Song> result = new ArrayList<>();

        for (Song song : songs) {
            if (song.getAuthor().equals(author)) {
                result.add(song);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "title='" + title + '\'' +
                ", songs=" + songs +
                '}';
    }
}
