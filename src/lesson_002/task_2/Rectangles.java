package lesson_002.task_2;

import java.util.ArrayList;
import java.util.List;

public class Rectangles {
    private final List<Rectangle> rectangles = new ArrayList<>();

    public void addRectangle(Rectangle rectangle) {
        rectangles.add(rectangle);
    }

    public int countSummarySquare() {
        int summary = 0;
        for (Rectangle rectangle : rectangles) {
            summary += rectangle.square();
        }
        return summary;
    }
}

