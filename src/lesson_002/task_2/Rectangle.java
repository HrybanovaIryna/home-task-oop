package lesson_002.task_2;

public class Rectangle {
    private final int width;
    private final int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int perimeter() {
        return (width + height) * 2;
    }

    public int square() {
        return height * width;
    }
}