package lesson_002.task_2;

public class RectangleRunner {
    public static void main(String[] args) {

        Rectangle rectangle1 = new Rectangle(3, 4);
        Rectangle rectangle2 = new Rectangle(5, 4);
        Rectangle rectangle3 = new Rectangle(2, 5);

        System.out.println("perimeter rectangle1: ");
        System.out.println(rectangle1.perimeter());
        System.out.println("height rectangle2: ");
        System.out.println(rectangle2.getHeight());
        System.out.println("width rectangle3: ");
        System.out.println(rectangle3.getWidth());

        System.out.println("square rectangle1: ");
        System.out.println(rectangle1.square());

        Rectangles rectangles = new Rectangles();
        rectangles.addRectangle(rectangle1);
        rectangles.addRectangle(rectangle2);
        rectangles.addRectangle(rectangle3);

        System.out.println("summary square: ");

        System.out.println(rectangles.countSummarySquare());


    }
}
