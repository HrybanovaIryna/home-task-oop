package lesson_002.task_1;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private final String name;
    private final List<Dish> dishes = new ArrayList<>();

    public Menu(String name) {
        this.name = name;
    }

    public void addDish(Dish dish) {
        dishes.add(dish);
    }

    public Dish findCheapest() {
        Dish cheapest = dishes.get(0);
        for(Dish dish : dishes) {
            if(cheapest.getPrice() > dish.getPrice()) {
                cheapest = dish;
            }
        }
        return cheapest;
    }
}
