package lesson_002.task_1;

public class MenuRunner {
    public static void main(String[] args) {

        Menu desserts = new Menu("Desserts");

        Dish cake = new Dish("Cake", "chocolate cake", 50);
        Dish iceCream = new Dish("ice cream", "vanilla", 20);
        Dish fruits = new Dish("fruits", "bananas, oranges, apples", 40);

        desserts.addDish(cake);
        desserts.addDish(iceCream);
        desserts.addDish(fruits);

        Menu drinks = new Menu("Drinks");

        Dish vine = new Dish("vine", "red vine", 100);
        Dish champagne = new Dish("vintage", "red", 120);
        Dish lemonade = new Dish("lemonade", "with mint", 20);

        drinks.addDish(vine);
        drinks.addDish(champagne);
        drinks.addDish(lemonade);

        System.out.println(desserts.findCheapest());
        System.out.println(drinks.findCheapest());
    }
}

