package lesson_002.task_3;


public class Line {
    private final Point firstPoint;
    private final Point lastPoint;

    public Line(Point firstPoint, Point lastPoint) {
        this.firstPoint = firstPoint;
        this.lastPoint = lastPoint;
    }

    public double returnLength() {
        return Math.sqrt(Math.pow((firstPoint.getX() - lastPoint.getX()), 2) + Math.pow((firstPoint.getY() - lastPoint.getY()), 2));
    }

    @Override
    public String toString() {
        return "Line{" +
                "firstPoint=" + firstPoint +
                ", lastPoint=" + lastPoint +
                '}';
    }
}
