package lesson_002.task_3;

import java.util.ArrayList;
import java.util.List;

public class Lines {
    private final List<Line> lineList = new ArrayList<>();

    public void addLine(Line line) {
        lineList.add(line);
    }

    public int summaryLine() {
        int summary = 0;
        for (Line line : lineList) {
            summary += line.returnLength();
        }
        return summary;
    }

    public Line findBiggest() {
        Line biggest = lineList.get(0);
        for (Line line : lineList) {
            if (biggest.returnLength() > line.returnLength()) {
                biggest = line;
            }
        }
        return biggest;
    }
}
