package lesson_002.task_3;

public class LineRunner {
    public static void main(String[] args) {
        Point firstPoint = new Point(2, 3);
        Point secondPoint = new Point(4, 5);
        Point thirdPoint = new Point(2,6);
        Point forthPoint = new Point(-6, 7);
        Point fifthPoint = new Point(3, 5);
        Point sixPoint = new Point(4, -7);

        Line line1 = new Line(firstPoint,secondPoint);
        Line line2 = new Line(thirdPoint, forthPoint);
        Line line3 = new Line(fifthPoint, sixPoint);

        System.out.println(line1.returnLength());

        Lines lines = new Lines();
        lines.addLine(line1);
        lines.addLine(line2);
        lines.addLine(line3);

        System.out.println(lines.summaryLine());
        System.out.println(lines.findBiggest());

    }
}
