package lesson_002.task_4;

public class LibraryRunner {
    public static void main(String[] args) {

        Book book1 = new Book("Fahrenheit 451", "Ray Bradbury");
        book1.setPublishingHouse("Simon & Schuster");
        book1.setYear(1953);
        book1.setPages(249);
        book1.setPrice(8.99);
        book1.setBindingType("paperbacks");

        Book book2 = new Book("Harry Potter and the Sorcerer’s Stone", "Rowling");
        book2.setPublishingHouse("Raincoast");
        book2.setYear(1997);
        book2.setPages(223);
        book2.setPrice(25.49);
        book2.setBindingType("hardcover");

        Book book3 = new Book("Harry Potter and the Chamber of Secrets", "Rowling");
        book3.setPublishingHouse("Bloomsbury");
        book3.setYear(1998);
        book3.setPages(251);
        book3.setPrice(31.99);
        book3.setBindingType("hardcover");

        Book book4 = new Book("The Book Thief", "Markus Zusak");
        book4.setPublishingHouse("Alfred A. Knopf");
        book4.setYear(2005);
        book4.setPages(608);
        book4.setPrice(13.99);
        book4.setBindingType("hardcover");

        Library classicLibrary = new Library();
        classicLibrary.addBooks(book1, book2, book3, book4);
        System.out.println(classicLibrary.findByWriter("Rowling"));
        System.out.println(classicLibrary.findByPublishingHouse("Alfred A. Knopf"));
        System.out.println(classicLibrary.findAfterSpecificYear(2000));

    }
}
