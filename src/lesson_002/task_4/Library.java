package lesson_002.task_4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addBooks(Book... arrayBook) {
        books.addAll(Arrays.asList(arrayBook));
    }

    public List<Book> findByWriter(String writer) {
        List<Book> result = new ArrayList<>();

        for (Book book : books) {
            if (book.getWriter().equals(writer)) {
                result.add(book);
            }
        }
        return result;
    }

    public List<Book> findByPublishingHouse(String publishingHouse) {
        List<Book> result = new ArrayList<>();

        for (Book book : books) {
            if (book.getPublishingHouse().equals(publishingHouse)) {
                result.add(book);
            }
        }
        return result;
    }

    public List<Book> findAfterSpecificYear(int year) {
        List<Book> result = new ArrayList<>();

        for (Book book : books) {
            if (book.getYear() > year) {
                result.add(book);
            }
        }
        return result;
    }
}
