package lesson_001;

import java.util.ArrayList;
import java.util.Arrays;

public class LongestNameFinder {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<>(Arrays.asList("Phil", "Anny", "Richard", "Chris"));
        System.out.println(longestName(names));
    }

    public static String longestName(ArrayList<String> names) {

        if (names.isEmpty()) {
            throw new IllegalArgumentException("The List is empty");
        }
        String longestName = "";

        for (String name : names) {
            if (name.length() > longestName.length()) {
                longestName = name;
            }
        }
        return longestName;
    }

}
