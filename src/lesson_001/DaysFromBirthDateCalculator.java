package lesson_001;

import java.time.LocalDate;
import java.time.temporal.ChronoField;

public class DaysFromBirthDateCalculator {
    public static void main(String[] args) {
        System.out.println(daysFromBirthDay("1995-02-11"));

    }

    public static long daysFromBirthDay(String birthDateString) {
        LocalDate now = LocalDate.now();
        LocalDate birthDate = LocalDate.parse(birthDateString);
        return now.getLong(ChronoField.EPOCH_DAY) - birthDate.getLong(ChronoField.EPOCH_DAY);
    }
}
