package lesson_001;

import java.util.ArrayList;
import java.util.Arrays;

public class NamesWithStartLetterFinder {
    public static void main(String[] args) {
        ArrayList<String> names = new ArrayList<>(Arrays.asList("Phil", "Anny", "Aaron", "Bob"));
        System.out.println(findNameWithStartLetter(names, 'A'));
    }

    public static ArrayList<String> findNameWithStartLetter(ArrayList<String> names, char startLetter) {
        ArrayList<String> newNames = new ArrayList<>();

        for (String name : names) {
            if (name.charAt(0) == startLetter) {
                newNames.add(name);
            }
        }
        return newNames;
    }
}
