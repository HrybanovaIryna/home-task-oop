package lesson_004.task_2;

import java.util.Date;

public class DialogRunner {
    public static void main(String[] args) {

        Message message1 = new Message("Petrov", new Date(1517781600000L), new PictureText("like"));
        Message message2 = new Message("Ivanov", new Date(1514782600000L), new EmoticonText("ghost"));
        Message message3 = new Message("Sidorov", new Date(1524782600000L), new PlainText("Hi!"));

        Dialog dialog = new Dialog();
        dialog.addMessage(message1);
        dialog.addMessage(message2);
        dialog.addMessage(message3);

        dialog.printMessages();
    }
}
