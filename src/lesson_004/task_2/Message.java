package lesson_004.task_2;

import java.util.Date;

public class Message {
    private final String author;
    private final Date dataOfPublication;
    private final Text text;

    public Message(String author, Date dataOfPublication, Text text) {
        this.author = author;
        this.dataOfPublication = dataOfPublication;
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public Date getDataOfPublication() {
        return dataOfPublication;
    }

    public Text getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Message{" +
                "author='" + author + '\'' +
                ", dataOfPublication=" + dataOfPublication +
                ", text='" + text.getText() + '\'' +
                '}';
    }
}
