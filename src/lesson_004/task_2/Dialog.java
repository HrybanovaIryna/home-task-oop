package lesson_004.task_2;

import java.util.LinkedHashSet;
import java.util.Set;

public class Dialog {
    private final Set<Message> messages = new LinkedHashSet<>();

    public void addMessage(Message message) {
        messages.add(message);
    }

    public void printMessages() {
        for (Message message : messages) {
            System.out.println(message);
        }
    }
}