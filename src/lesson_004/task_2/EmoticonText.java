package lesson_004.task_2;

public class EmoticonText implements Text {

    private final String emoticon;

    public EmoticonText(String text) {
        if ("winking-face".equals(text)) {
            emoticon = "😉";
        } else if ("ghost".equals(text)) {
            emoticon = "\uD83D\uDC7B";
        } else {
            throw new IllegalArgumentException("no emoticon for text: " + text);
        }
    }

    @Override
    public String getText() {
        return emoticon;
    }
}
