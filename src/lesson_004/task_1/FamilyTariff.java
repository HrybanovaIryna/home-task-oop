package lesson_004.task_1;

public class FamilyTariff implements Tariff {

    @Override
    public long calculatePrice(Ride ride) {
        return 50 +20 * ride.getDistance() / ride.getPassengers();
    }

    @Override
    public String toString() {
        return "FamilyTariff{}";
    }
}
