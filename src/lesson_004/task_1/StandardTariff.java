package lesson_004.task_1;

public class StandardTariff implements Tariff {

    @Override
    public long calculatePrice(Ride ride) {
        return 30 + 20 * ride.getDistance() + 2 * ride.getDuration();
    }

    @Override
    public String toString() {
        return "StandardTariff{}";
    }
}
