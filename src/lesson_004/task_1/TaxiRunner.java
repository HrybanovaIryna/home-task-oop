package lesson_004.task_1;

public class TaxiRunner {
    public static void main(String[] args) {

        Tariff standardTariff = new StandardTariff();
        Tariff familyTariff = new FamilyTariff();

        Ride ride1 = new Ride(2,20, 20, standardTariff);
        Ride ride2 = new Ride(5,30, 40, familyTariff);
        Ride ride3 = new Ride(2,15, 15, standardTariff);

        RidesHistory ridesHistory = new RidesHistory();

        System.out.println("Get price " + ride1.getPrice());
        ridesHistory.addRide(ride1);
        ridesHistory.addRide(ride2);
        ridesHistory.addRide(ride3);
        System.out.println(" Total price: " + ridesHistory.getTotalPrice());

        System.out.println("History: " + ridesHistory);

    }
}
