package lesson_004.task_1;

public interface Tariff {
    long calculatePrice(Ride ride);
}

